from django.contrib.auth.models import User
from rest_framework import serializers

from shop.models import Item


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'is_staff')


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('title', 'slug', 'description', 'price', 'category', 'image')
