from django.conf.urls import url
from django.conf.urls.static import static
from main import settings
from shop import views

urlpatterns = [
    url(r'^category/(?P<gender>\w+)/(?P<slug>\w+)$', views.CategoryView.as_view(), name='category'),
    url(r'^about/', views.AboutView.as_view(), name='about'),
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'^feedback/$', views.Feedback.as_view(), name='feedback'),
    url(r'^categories/$', views.Categories.as_view(), name='categories'),

    url(r'^(?P<category_slug>\w+)/(?P<slug>[-_\w]+)$', views.ItemDetailView.as_view(), name='item_detail'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
