from django.core.mail import send_mail
from django.template.loader import render_to_string


class EmailSender(object):
    subject = None
    path_to_html_template = None
    path_to_text_template = None
    context = None

    def __init__(self, template='', context=None):
        self.recipients = []
        self.path_to_html_template = '%s.html' % template
        self.path_to_text_template = '%s.txt' % template
        self.subject = '%s.txt' % template
        self.context = context

    def add_recipient(self, recipient):
        self.recipients.append(recipient)

    def remove_recipient(self, recipient):
        self.recipients.remove(recipient)

    def get_subject(self):
        return render_to_string(self.subject)

    def get_html_message(self):
        return render_to_string(self.path_to_html_template, self.context)

    def get_text_message(self):
        return render_to_string(self.path_to_text_template, self.context)

    def send(self):
        text_message = self.get_text_message()
        html_message = self.get_html_message()
        subject = self.get_subject()
        send_mail(subject, text_message, 'Feedback <support@localhost>', self.recipients, html_message=html_message)
