from django import forms

from django.core.mail import send_mail

from main import settings
from shop.models import Feedback


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['name', 'email', 'text']

    def send_email(self):
        name = self.cleaned_data['name']
        email = self.cleaned_data['email']
        text = self.cleaned_data['text']

        body = """\
                Name: %s
                From: %s
                Text: %s""" % (name, email, text)

        recipients = ['polu4atel']
        send_mail('Feedback', body, settings.DEFAULT_FROM_EMAIL, recipients)

