from django.contrib import admin
from shop.models import Category, Item, Slider, Feedback, Geoposition


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'gender']


class ItemAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'description', 'price', 'category']


class SliderAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'image']


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'text', 'date']


class GeopositionAdmin(admin.ModelAdmin):
    list_display = ['name', 'position', 'position_map', ]

    def position_map(self, instance):
        if instance.position is not None:
            return '<img src="http://maps.googleapis.com/maps/api/staticmap?center=%(latitude)s,%(longitude)s&zoom=%(zoom)s&size=%(width)sx%(height)s&maptype=roadmap&markers=%(latitude)s,%(longitude)s&sensor=false&visual_refresh=true&scale=%(scale)s" width="%(width)s" height="%(height)s">' % {
                'latitude': instance.position.latitude,
                'longitude': instance.position.longitude,
                'zoom': 15,
                'width': 100,
                'height': 100,
                'scale': 2
            }
    position_map.allow_tags = True


admin.site.register(Category, CategoryAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Slider, SliderAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(Geoposition, GeopositionAdmin)
