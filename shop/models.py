# coding=utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from geoposition.fields import GeopositionField


class Category(models.Model):
    SEX_CHOICES = (
        ('women', _('Женские')),
        ('man', _('Мужские')),
        ('unisex', _('Унисекс'))
    )

    name = models.CharField(max_length=30, verbose_name=_('Название'))
    slug = models.CharField(max_length=30, default='', verbose_name=_('Метка'))
    gender = models.CharField(choices=SEX_CHOICES, default='', max_length=6, verbose_name=_('Пол'))

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = _('Категории')
        verbose_name = _('категорию')


class Item(models.Model):
    title = models.CharField(max_length=50, verbose_name=_('Заголовок'))
    slug = models.CharField(max_length=50, default='', verbose_name=_('Метка'))
    description = models.TextField(verbose_name=_('Описание'))
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_('Цена'))
    category = models.ForeignKey(Category, related_name='items', blank=True, null=True, verbose_name=_('Категория'))
    image = models.ImageField(upload_to='items', blank=True, null=True, verbose_name=_('Изображение'))

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name_plural = _('Товары')
        verbose_name = _('товар')


class Slider(models.Model):
    image = models.ImageField(upload_to='slider', verbose_name=_('Изображение'))
    title = models.CharField(max_length=50, verbose_name=_('Заголовок'))
    description = models.TextField(verbose_name=_('Описание'))

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name_plural = _('Слайды')
        verbose_name = _('слайд')


class Feedback(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('Имя'))
    email = models.EmailField(verbose_name=_('Эл почта'))
    text = models.TextField(verbose_name=_('Текст'))
    date = models.DateTimeField(auto_now_add=True, verbose_name=_('Дата и время'))

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = _('Отзывы')
        verbose_name = _('отзыв')


class Geoposition(models.Model):
    name = models.CharField(max_length=200, verbose_name=_('Название'))
    position = GeopositionField()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Геопозиция')
        verbose_name_plural = _('Геопозиции')
