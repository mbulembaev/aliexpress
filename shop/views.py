# coding=utf-8
from __future__ import unicode_literals

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.views.generic import DetailView, TemplateView, FormView
from rest_framework import viewsets

from shop.forms import FeedbackForm
from shop.models import Category, Item, Slider, Geoposition
from django.utils.translation import ugettext_lazy as _

from shop.serializers import UserSerializer, ItemSerializer


class CategoryView(TemplateView):
    template_name = 'category.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.get(gender=self.kwargs['gender'], slug=self.kwargs['slug'])
        return context


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['slides'] = Slider.objects.all()
        context['categories'] = Category.objects.all()
        context['items'] = Item.objects.all()
        return context


class Categories(TemplateView):
    template_name = 'categories.html'
    model = Category

    def get_context_data(self, **kwargs):
        context = super(Categories, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context


class AboutView(TemplateView):
    template_name = 'about_us.html'


class ItemDetailView(DetailView):
    template_name = 'item_detail.html'
    model = Item
    slug_field = 'slug'


class Feedback(FormView):
    template_name = 'contact.html'
    form_class = FeedbackForm
    success_url = 'http://localhost:8000/feedback/'

    def get_context_data(self, **kwargs):
        context = super(Feedback, self).get_context_data(**kwargs)
        context['position'] = Geoposition.objects.get(pk=1)
        return context

    def form_valid(self, form):
        form.save()
        form.send_email()
        messages.add_message(self.request, messages.SUCCESS, _('Ваше сообщение успешно отправлено!'))
        return super(Feedback, self).form_valid(form)


class ProfileView(TemplateView):
    template_name = 'profile.html'


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
